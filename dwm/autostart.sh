#!/bin/bash

# fcitx
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFILERS=@im=fcitx

# wmname
export _JAVA_AWT_WM_NONREPARENTING=1
export AWT_TOOLKIT=MToolkit
wmname LG3D

picom &
fcitx5 &

/bin/bash ~/scripts/dwm/dwm-status.sh &
/bin/bash ~/scripts/dwm/dwm-wallpaper.sh &
